import "./css/main.scss";

// import $ from 'jquery';
import _ from 'lodash';
// import moment from 'moment';
// moment.locale('de');
import M from "materialize-css";
// import { Grid } from "gridjs";
const shaka = require("shaka-player");
// import shaka from 'shaka-player'
// import { auth } from '@upstash/redis';

// auth('https://eu1-wired-hog-33433.upstash.io', 'AYKZASQgYzk4YzU4YTQtOGUxNy00ZDViLTlkNmQtODBjZjk3NjRhNTQ0NTIwNmU1MzM0Yzc2NDdlOGI5ZDA0YjU3OGYyM2IwMDk=');


let nImg = 0;
const players: any[] = [];


const manifestUri =
    'https://fedora.local/dash/';
const streams = ['ir', 'lr'];

async function initApp() {
    // Install built-in polyfills to patch browser incompatibilities.
    shaka.polyfill.installAll();

    // Check to see if the browser supports the basic APIs Shaka needs.
    if (!shaka.Player.isBrowserSupported()) {
        // This browser does not have the minimum set of APIs we need.
        throw new Error('Browser not supported!');
    }
}

async function initPlayer(n = 0) {
    // Create a Player instance.
    const video = document.getElementById('video' + (n+1));
    const player = new shaka.Player(video);

    // Attach player to the window to make it easy to access in the JS console.
    players[n] = player;
    // _.set(window, 'players', players);
    // Listen for error events.
    player.addEventListener('error', onErrorEvent);

    // Try to load a manifest.
    // This is an asynchronous process.
    try {
        await player.load(`${manifestUri}${streams[n]}.mpd`);
        // This runs if the asynchronous load is successful.
        console.log('The video has now been loaded!');
    } catch (e) {
        // onError is executed if the asynchronous load fails.
        onError(e);
    }
}

function onErrorEvent(event: any) {
    // Extract the shaka.util.Error object from the event.
    onError(event.detail);
}

function onError(error: any) {
    // Log the error.
    console.error('Error code', error.code, 'object', error);
}

document.addEventListener('DOMContentLoaded', async function () {

    await initApp();

    await initPlayer(0);
    await initPlayer(1);
    
    // await Promise.all(_.times(streams.length, initPlayer));
    _.set(window, 'players', players);
});

M.AutoInit();
